set PATH ~/go/bin $PATH

switch (uname)
case Linux
	source ~/.asdf/asdf.fish
	set ANDROID_HOME /data/jmtan/Android/sdk
	set PATH /usr/local/go/bin ~/.config/yarn/global/node_modules/.bin ~/flutter/bin $ANDROID_HOME/emulator $ANDROID_HOME/tools $ANDROID_HOME/platform-tools $PATH
	set RA ~/Workspace
	set BROWSER x-www-browser
	set _JAVA_AWT_WM_NONREPARENTING 1
case Darwin
	set ANDROID_HOME ~/Library/Android/sdk
	set JAVA_HOME (/usr/libexec/java_home -v 1.8)
	set PATH $JAVA_HOME/bin /usr/local/opt/python@2/bin ~/flutter/bin ~/.local/node-v8.11.3-darwin-x64/bin $ANDROID_HOME/emulator $ANDROID_HOME/tools $ANDROID_HOME/platform-tools ~/.cargo/bin $PATH
	set RA ~/Workspace/radioactive
	set BROWSER open
end
set PATH ~/.emacs.d/bin $PATH

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showupstream 'auto'
set __fish_git_prompt_char_stateseparator '|'

set -U fish_features qmark-noglob
