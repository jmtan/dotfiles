function fish_title
	if [ "" != "$TMUX" ]
		if [ "fish" != $_ ]
			tmux rename-window "$_ $argv"
		else
			tmux_directory_title
		end
	end
end
