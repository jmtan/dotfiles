function git_home_page
	git remote -v | head -n 1 | awk '{print $2}' | sed s/^git@// | sed s/.git\$// | sed s/:/\\// | xargs printf 'https://%s'
end
