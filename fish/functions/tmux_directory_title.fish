# http://pedroassuncao.com/fish-shell-how-to-set-tmux-window-slash-pane-title-based-on-current-command-and-directory
function tmux_directory_title
	if [ "$PWD" != "$LPWD" ]
		set LPWD "$PWD"
		set INPUT $PWD
		set SUBSTRING (eval echo $INPUT| awk '{ print substr( $0, length($0) - 19, length($0) ) }')
		tmux rename-window "..$SUBSTRING"
	end
end
