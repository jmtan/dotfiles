# Defined in - @ line 2
function gosys
	set sys (http "http://localhost:6060/debug/pprof/heap?debug=1" | grep "# Sys" | awk '{print $4}')
    set mb (math $sys / 1048576)
    set ts (date '+%H:%M:%S')
    echo "$ts - sys: $sys ($mb mb)"
end
