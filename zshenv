export LIBRARY_PATH="$LIBRARY_PATH:/usr/local/lib"
export PATH=/usr/local/bin:~/go/bin:$PATH

platform=$(uname)

if [[ "$platform" == "Darwin" ]]; then
    export PATH="/usr/local/opt/python/libexec/bin:/usr/local/opt/go/libexec/bin:/Applications/Emacs.app/Contents/MacOS/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:$HOME/.cargo/bin:$HOME/go/bin:$HOME/.local/bin:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"
else
    export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/local/go/bin:$HOME/go/bin:$HOME/.local/bin:$HOME/.npm-global/bin:$HOME/.config/yarn/global/node_modules/.bin"
fi

export MANPATH="/usr/local/man:$MANPATH"

export ANSIBLE_CACHE_MAX_AGE=99999

export ANDROID_HOME=/data/jmtan/Library/Android/sdk

export NPM_CONFIG_PREFIX=~/.npm-global
