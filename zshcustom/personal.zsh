# My Configuration

# shell
alias re=". ~/.zshrc"
alias ex="thunar . &"
alias er="emacsclient -t ~/dotfiles/zshcustom/personal.zsh"
alias eer="emacsclient -t ~/dotfiles/zshrc"
if [[ "$platform" == "Darwin" ]]; then
    alias emacs="/Applications/Emacs.app/Contents/MacOS/Emacs"
fi
if [[ "$platform" == "Darwin" ]]; then
    alias ls="ls -G"
    alias l="ls -G"
else
    alias ls="ls --color=auto"
    alias l="ls --color=auto"
fi
alias z="cd -"
alias cdws="cd ~/Workspace"
alias less="less -R"
if [[ "$platform" != "Darwin" ]]; then
    alias pbcopy="xclip -selection clipboard"
    alias pbpaste="xclip -selection clipboard -o"
fi
alias sdr="pwd | pbcopy"
alias cdr="cd \$(pbpaste)"
function take() {
    mkdir -p $1 && cd $1
}
function b() {
    cd $* && ls
}
alias lsdir="ls -l | grep ^d | awk '{print \$9}' | column"


# emacs
alias e="emacsclient -t"
alias ee="emacsclient -nc"
alias killemacs="emacsclient -e \"(kill-emacs)\""

# tmux
alias t="tmux"
alias tl="t list-sessions"
alias ta="t attach"
alias te="e ~/.tmux.conf"

# Git
alias g="git status"
alias ga="git add"
alias gb="git branch"
alias gc="git checkout"
alias gl="git log"
alias gd="git diff"
alias gg="nohup gitg > /dev/null &"
alias gp="git pull"
alias ginc="git fetch && git log HEAD..origin"
alias gout="git log origin..HEAD"
alias gpp="git push"
function gcm () {
    git commit -m "$*"
}
function gcam () {
    git commit -a -m "$*"
}
function gitpers () {
    git config --global user.name "Eugene Tan"
    git config --global user.email "jmingtan@gmail.com"
}
function gitwork () {
    git config --global user.name "Eugene Tan"
    git config --global user.email "eugene@radioactive.sg"
}
function gituser () {
    git config --global user.name
    git config --global user.email
}

# Fabric
alias f="fab"
alias fl="fab -l"
alias fs="fab setup_test"
alias ft="fab tear_down_test"
alias fate="fab tear_down_test"
alias fase="fab setup_test"

# Docker
alias d="docker"
alias dps="docker ps"
alias dpsa="docker ps -a"
alias dk="docker kill \$(docker ps -q)"
alias drm="docker rm \$(docker ps -aq)"
alias dim="docker images"
alias dcm="docker-compose"
alias dstats="docker stats \$(docker ps | tail -n +2 | awk '{print $NF}')"

# ssh
alias sshd="ssh -D 8080"
function sshr () {
    ssh -i ~/.ssh/keys/radioactive.pem -o StrictHostKeyChecking=no ubuntu@$1
}
function sshk () {
    ssh -i ~/.ssh/keys/ra_id_rsa -o StrictHostKeyChecking=no ubuntu@$1
}
function ssho () {
    ssh -i keys/digitalocean_key -o StrictHostKeyChecking=no root@$1
}

# aptitude
alias api="sudo aptitude install"
alias aps="aptitude search"
alias apsh="aptitude show"

# ansible
alias an="ansible"
alias ani="ansible -i inventory/hosts"
alias anpl="ansible-playbook"
alias anpli="ansible-playbook -i inventory/hosts"

# kubernetes
alias kc="kubectl"

# packer
alias pk="packer"

# terraform
alias tf="terraform"

# openbox helpers
alias obspy="obxprop | grep ^_OB_APP"

function ram () {
    mplayer "$1?authtoken=$(ra_streamtoken)"
}

# Misc
function cachecheck () {
    x-www-browser $1/oz/public/oz-bandung/
}
function cachecheck2 () {
    x-www-browser "$1/amp/getConfig.php?app=hitz_mobile&deviceId=46DB4E930AC2401FB73BCB623155CAF7&deviceType=iphone&api=5.0.0&appVersion=6.0.2&noads=1"
}

# lookup
function doc-http () {
    if [ -z "$1" ]
    then
        x-www-browser "https://en.wikipedia.org/wiki/List_of_HTTP_status_codes"
    else
        x-www-browser "https://en.wikipedia.org/wiki/HTTP_$1"
    fi
}

function doc-play23api () {
    if [ -z "$1" ]
    then
        x-www-browser "https://www.playframework.com/documentation/2.3.x/api/scala/index.html#package"
    else
        x-www-browser "https://www.playframework.com/documentation/2.3.x/api/scala/index.html#$1"
    fi
}

function doc-play24api () {
    if [ -z "$1" ]
    then
        x-www-browser "https://www.playframework.com/documentation/2.4.x/api/scala/index.html#package"
    else
        x-www-browser "https://www.playframework.com/documentation/2.4.x/api/scala/index.html#$1"
    fi
}

alias doc-play23="x-www-browser https://www.playframework.com/documentation/2.3.x/Home"

function doc-scala () {
    if [ -z "$1" ]
    then
        x-www-browser "http://www.scala-lang.org/api/current/#package"
    else
        x-www-browser "http://www.scala-lang.org/api/current/#$1"
    fi
}

alias doc-jodatime="x-www-browser http://joda-time.sourceforge.net/apidocs"

function doc-elastic () {
    if [ -z "$1" ]
    then
        x-www-browser "https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html"
    else
        x-www-browser "https://www.elastic.co/search?q=$*"
    fi
}

alias doc-logstash="x-www-browser https://www.elastic.co/guide/en/logstash/current/index.html"

alias doc-kibana="x-www-browser https://www.elastic.co/guide/en/kibana/current/index.html"

alias doc-ansible="x-www-browser https://docs.ansible.com/ansible/index.html"

alias doc-varnish="x-www-browser https://www.varnish-cache.org/docs/4.0/"

alias m="mplayer"

alias jpop="mplayer http://198.178.123.14:7002/stream/1/"
