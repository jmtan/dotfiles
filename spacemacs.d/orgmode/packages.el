;;; packages.el --- orgmode Layer packages File for Spacemacs
;;
;; Copyright (c) 2012-2014 Sylvain Benner
;; Copyright (c) 2014-2015 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;; List of all packages to install and/or initialize. Built-in packages
;; which require an initialization must be listed explicitly in the list.
(setq orgmode-packages
    '(
      ;; package orgmodes go here
      org

      (org :location built-in)
      (org-plus-contrib :step pre)
      ))

;; List of packages to exclude.
(setq orgmode-excluded-packages '())

(defun org-find-sibling-node (search-str)
  "Find a same-level node using given search string"
  (interactive "p")
  (let ((find (lambda ()
                (equal (org-get-heading t t) search-str))))
    (if (funcall find)
        t
      (while (if (org-goto-sibling)
                 (not (funcall find)))))))

(defun org-find-latest-entry ()
  "Find the latest journal entry in a tree"
  (interactive)
  (let ((year (format-time-string "%Y"))
        (month (format-time-string "%B"))
        (day (format-time-string "%d")))
    (unless (org-at-heading-p)
      (org-forward-heading-same-level nil t))
    (org-find-sibling-node year)
    (org-cycle)
    (org-goto-first-child)
    (org-find-sibling-node month)
    (org-cycle)
    (org-goto-first-child)
    (org-find-sibling-node day)
    (org-cycle)))

(defun org-insert-latest-entry ()
  "Insert a new journal entry into a tree"
  (interactive)
  (org-end-of-line)
  (org-insert-heading-respect-content t)
  (insert (format-time-string "%H%M")))

(defun orgmode/init-org ()
  (use-package org
    :commands org-defkey
    :mode ("\\.org$" . org-mode)
    :defer t
    :init
    (setq org-src-fontify-natively t)
    :config
    (setq electric-indent-mode nil)
    (org-defkey org-mode-map "\C-c\C-i" 'org-insert-latest-entry)
    (org-defkey org-mode-map "\C-c\C-g" 'org-find-latest-entry)
    (add-hook 'org-mode-hook 'turn-on-auto-fill)
    ))
