" vim plugins
" ===========
" first, make sure vimplug is installed with:
"
"  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" within vim, invoke :PlugInstall to setup plugins
"
" for more info: https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')

" fuzzy finder, use <ctrl>p to open
Plug 'junegunn/fzf'
" ack interface (faster grep)
Plug 'mileszs/ack.vim'
" show git changes in gutter
Plug 'airblade/vim-gitgutter'
" <Leader><Leader>w to jump to any word
Plug 'easymotion/vim-easymotion'
" useful system commands e.g. :Chmod, :SudoWrite
Plug 'tpope/vim-eunuch'
" easily modify surrounding brackets
Plug 'tpope/vim-surround'
" git interface
Plug 'tpope/vim-fugitive'
" cooler status bar
Plug 'vim-airline/vim-airline'
" additional syntax
Plug 'sheerun/vim-polyglot'
" brackets coloured by nesting level
Plug 'luochen1990/rainbow'
" comment/uncomment code with <leader>c<space>
Plug 'scrooloose/nerdcommenter'
" file explorer sidebar, open with <ctrl>n
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
" clojure nrepl client
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
" automagically add/remove parentheses by indentation
" Plug 'bhurlow/vim-parinfer', { 'for': 'clojure' }
" automagically add/remove parentheses by indentation, requires rust
Plug 'eraserhd/parinfer-rust', { 'for': 'clojure', 'do': 'cargo build --release' }

call plug#end()

" nerdtree: use ctrl-o to open
:nmap <C-n> :NERDTreeToggle<CR>

" rainbow parentheses: turn on globally
let g:rainbow_active = 1

" gitgutter: make updatetime shorter
set updatetime=100

" keybinding to open .vimrc in split
:nmap <leader>e :sp ~/.vimrc<CR>

" keybinding for reload
:nmap <leader>r :source ~/.vimrc<CR>

" fzf: use ctrl-p to open
:nmap <C-p> :FZF<CR>

" turn off word wrap
set nowrap

" turn on incremental search
set incsearch
