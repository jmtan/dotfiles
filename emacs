;; -*- mode: emacs-lisp -*-

(require 'cl)

(defvar *emacs-load-start* (current-time))

(defun user-print-benchmark (msg start-time)
  (message msg
	 (destructuring-bind (hi lo usec psec) (current-time)
	   (- (+ hi lo)
	      (+ (first start-time) (second start-time))))))

(defun benchmark-load-library (path)
  (let ((result (benchmark-run (load path))))
    (message "%s loaded in %fs" path (first result))))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq use-package-verbose t)

;; general keybindings
(global-set-key [(control down)] (lambda () (interactive) (scroll-up 1)))
(global-set-key [(control up)] (lambda () (interactive) (scroll-down 1)))
(global-unset-key (kbd "C-z"))

(user-print-benchmark ".emacs loaded in %ds (before additional config)" *emacs-load-start*)

;; additional config files
(benchmark-load-library "~/dotfiles/elisp/shell.el")
(benchmark-load-library "~/dotfiles/elisp/editing.el")
(benchmark-load-library "~/dotfiles/elisp/lang.el")
(benchmark-load-library "~/dotfiles/elisp/org-config.el")
(benchmark-load-library "~/dotfiles/elisp/project.el")
(benchmark-load-library "~/dotfiles/elisp/search.el")
(benchmark-load-library "~/dotfiles/elisp/ui.el")

;; custom.el settings
(setq custom-file "~/.emacs-custom.el")
(benchmark-load-library custom-file)

(user-print-benchmark ".emacs loaded in %ds" *emacs-load-start*)
