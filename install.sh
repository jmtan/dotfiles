#!/bin/bash

# http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/

dotfiles=~/dotfiles
backup=~/dotfiles_old
files="gitconfig tmux.conf vimrc emacs"
configdirs="openbox tint2 fish doom bspwm sxhkd"

rm -rf $backup
mkdir -p $backup
cd $dotfiles

echo "installing dotfiles in home dir"
for file in $files; do
  if [ -e ~/.$file ]
  then
    echo "backup ~/.$file to $backup/.$file"
    mv ~/.$file $backup
  fi
  ln -s $dotfiles/$file ~/.$file
done

echo "installing xdg configs in ~/.config/"
mkdir -p ~/.config
for configdir in $configdirs; do
  if [ -e ~/.config/$configdir ]
  then
    echo "backup ~/.config/$configdir to $backup/$configdir"
    mv ~/.config/$configdir $backup
  fi
  ln -s $dotfiles/$configdir ~/.config/$configdir
done

echo "install emacs straight.el lockfile"
mkdir -p ~/.emacs.d/straight
if [ -e ~/.emacs.d/straight/versions ]
then
  echo "backup ~/.emacs.d/straight/versions to $backup/versions"
  mv ~/.emacs.d/straight/versions $backup
fi
ln -s $dotfiles/straight ~/.emacs.d/straight/versions
