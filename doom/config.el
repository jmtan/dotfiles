;;; private/jmtan/config.el -*- lexical-binding: t; -*-

(def-package! terraform-mode)
(def-package! dart-mode)
(def-package! inf-clojure)

;; UI
(setq doom-font (font-spec :family "Source Code Pro" :size 14))
(setq doom-line-numbers-style nil) ;; turn off line numbers

(add-to-list 'default-frame-alist '(height . 50))
(add-to-list 'default-frame-alist '(width . 100))

;; File Associations
(push '("riemann\\.config$" . clojure-mode) auto-mode-alist)
(push '("\\.*Dockerfile$" . dockerfile-mode) auto-mode-alist)

(load! "+arcadia")

;; org
(setq org-agenda-files '("~/Dropbox/org" "/data/jmtan/Dropbox/org"))
