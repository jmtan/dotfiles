;; -*- no-byte-compile: t; -*-
;;; private/jmtan/packages.el


(setq package-archives (append package-archives
                               '(("melpa-stable" . "https://stable.melpa.org/packages/"))))

(package! terraform-mode)
(package! cider :pin "melpa-stable")
(package! dart-mode)
(package! inf-clojure)
