(use-package ag
  :commands ag
  :straight t)

(use-package avy
  :bind (("C-z C-z" . avy-goto-subword-1)
	 ("C-z z" . avy-goto-char))
  :config (avy-setup-default)
  :demand t
  :straight t)

(use-package ivy
  :bind (("C-x C-b" . ivy-switch-buffer)
	 ("C-x b" . ivy-switch-buffer))
  :config (progn (ivy-mode 1)
		 (setq ivy-use-virtual-buffers t
		       ivy-count-format "(%d/%d) "))
  :demand t
  :straight t)

(use-package swiper
  :commands swiper
  :after ivy
  :bind (("C-s" . swiper))
  :straight t)

(use-package counsel
  :after ivy org
  :config (with-eval-after-load 'org
	    (bind-keys
	     :map org-mode-map
	     ("C-c g" . counsel-org-goto)))
  :straight t)

(use-package ivy-prescient
  :after ivy
  :config (progn (ivy-prescient-mode)
		 (prescient-persist-mode))
  :straight t)
