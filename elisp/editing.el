(use-package smartparens
  :config (progn
            (require 'smartparens-config)
	          (smartparens-global-mode)
	          (show-smartparens-global-mode)
	          (sp-use-paredit-bindings)
	          (add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)
	          (add-hook 'eval-expression-minibuffer-setup-hook #'turn-on-smartparens-strict-mode)
	          (add-hook 'ielm-mode-hook #'turn-on-smartparens-strict-mode)
	          (add-hook 'lisp-mode-hook #'turn-on-smartparens-strict-mode)
	          (add-hook 'lisp-interaction-mode-hook #'turn-on-smartparens-strict-mode)
	          (add-hook 'scheme-mode-hook #'turn-on-smartparens-strict-mode)
	          (add-hook 'clojure-mode-hook #'turn-on-smartparens-strict-mode))
  :demand t
  :straight t)

(use-package modalka
  :config (progn (modalka-define-kbd "SPC" "C-SPC")
		             (modalka-define-kbd "a" "C-a")
		             (modalka-define-kbd "A" "M-a")
		             (modalka-define-kbd "b" "C-b")
		             (modalka-define-kbd "e" "C-e")
		             (modalka-define-kbd "E" "M-e")
		             (modalka-define-kbd "f" "C-f")
		             (modalka-define-kbd "g" "C-g")
		             (modalka-define-kbd "h" "C-h")
		             (modalka-define-kbd "n" "C-n")
		             (modalka-define-kbd "p" "C-p")
		             (modalka-define-kbd "q" "M-q")
		             (modalka-define-kbd "s" "C-s")
		             (modalka-define-kbd "w" "C-w")
		             (modalka-define-kbd "W" "M-w")
		             (modalka-define-kbd "y" "C-y")
		             (modalka-define-kbd "Y" "M-y")
		             (modalka-define-kbd "v" "C-v")
		             (modalka-define-kbd "V" "M-v")

		             (modalka-define-kbd "1" "C-1")
		             (modalka-define-kbd "2" "C-2")
		             (modalka-define-kbd "3" "C-3")
		             (modalka-define-kbd "4" "C-4")
		             (modalka-define-kbd "5" "C-5")
		             (modalka-define-kbd "6" "C-6")
		             (modalka-define-kbd "7" "C-7")
		             (modalka-define-kbd "8" "C-8")
		             (modalka-define-kbd "9" "C-9")
		             (global-set-key (kbd "<escape>") #'modalka-mode)
		             (global-set-key (kbd "C-,") #'modalka-mode)
		             (setq modalka-excluded-modes '(magit-status-mode
						                                    magit-popup-mode
						                                    magit-diff-mode
						                                    magit-revision-mode
                                                magit-log-mode
						                                    cider-inspector-mode
						                                    cider-repl-mode
                                                cider-test-report-mode
                                                cider-docview-mode
                                                bookmark-bmenu-mode
                                                debugger-mode
                                                nov-mode
						                                    ag-mode
						                                    Custom-mode
                                                dired-mode
						                                    help-mode
						                                    treemacs-mode
						                                    grep-mode
						                                    info-mode
						                                    eshell-mode
						                                    vc-annotate-mode))
		             (add-hook 'git-commit-setup-hook (lambda () (modalka-mode 0)))
		             (modalka-global-mode 1)
		             (setq-default cursor-type '(bar . 1))
		             (setq modalka-cursor-type 'hollow))
  :straight t)

(use-package lispy
  :hook (clojure-mode-hook emacs-lisp-mode-hook)
  :straight t)

(use-package easy-kill
  :config (progn (global-set-key [remap kill-ring-save] #'easy-kill)
                 (global-set-key [remap mark-sexp] #'easy-mark))
  :straight t)

(use-package uniquify
  :config (setq uniquify-buffer-name-style 'forward))

(setq-default truncate-lines t
	            scroll-conservatively 100
	            electric-indent-inhibit t
	            indent-tabs-mode nil
              tab-width 2
              backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))
(delete-selection-mode t)
(global-auto-revert-mode 1)
(show-paren-mode 1)
