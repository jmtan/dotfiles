(use-package company
  :init (add-hook 'after-init-hook 'global-company-mode)
  :bind ("TAB" . company-complete)
  :config (setq company-idle-delay 3)
  :straight t)

(use-package flycheck
  :defer t
  :straight t)

(use-package markdown-mode
  :mode "\\.md\\'"
  :straight t)

(use-package rust-mode
  :mode "\\.rs\\'"
  :straight t)

(use-package clojure-mode
  :mode ("\\.clj\\'" "\\.cljs\\'" "\\.edn\\'")
  :straight t)

(use-package cider
  :after (clojure-mode ivy)
  :config (progn (setq cider-repl-display-help-banner nil
		       cider-repl-pop-to-buffer-on-connect nil
		       cider-repl-use-pretty-printing t)
		 (add-hook 'cider-mode-hook #'eldoc-mode)
		 (add-hook 'cider-repl-mode-hook #'eldoc-mode))
  :straight t)

(use-package yaml-mode
  :mode ("\\.yaml\\'" "\\.yml\\'")
  :straight t)

(use-package terraform-mode
  :mode "\\.tf\\'"
  :straight t)

(use-package scala-mode
  :mode ("\\.scala\\'" "\\.sbt\\'")
  :straight t)

(use-package ensime
  :hook (scala-mode . ensime)
  :config (setq ensime-startup-notification nil)
  :straight (:host github :repo "ensime/ensime-emacs" :branch "2.0"))

(use-package fennel-mode
  :mode "\\.fnl\\'"
  :straight t)

(use-package lua-mode
  :mode "\\.lua\\'"
  :straight t)

(use-package groovy-mode
  :mode ("\\.gradle\\'" "\\.groovy\\'")
  :straight t)

(use-package omnisharp
  :after company
  :hook (csharp-mode . omnisharp-mode)
  :config (add-to-list 'company-backends 'company-omnisharp)
  :straight t)

(use-package shader-mode
  :mode "\\.shader\\'"
  :straight t)

(use-package go-mode
  :mode "\\.go\\'"
  :hook (before-save . gofmt-before-save)
  :config (progn
            (add-to-list 'exec-path "~/go/bin")
            (add-to-list 'exec-path "/usr/local/go/bin"))
  :straight t)

(use-package go-eldoc
  :after go-mode
  :hook (go-mode . go-eldoc-setup)
  :straight t)

(use-package company-go
  :after company go-mode
  :hook (go-mode . (lambda () (add-to-list 'company-backends 'company-go)))
  :straight t)

(use-package typescript-mode
  :mode ("\\.ts\\'" "\\.tsx\\'")
  :straight t)

(use-package tide
  :defer t
  :hook ((typescript-mode . tide-setup))
  :straight t)
