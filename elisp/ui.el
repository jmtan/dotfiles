(use-package doom-themes
  :config (load-theme 'doom-one t)
  :straight t)

(use-package spaceline
  :config (progn
            (require 'spaceline-config)
            (spaceline-spacemacs-theme)
            (setq spaceline-window-numbers-unicode t))
  :straight t)

(use-package which-key
  :config (which-key-mode t)
  :straight t)

(use-package git-gutter
  :config (global-git-gutter-mode t)
  :straight t)

(use-package nov
  :mode ("\\.epub\\'" . nov-mode)
  :straight t)

(use-package eyebrowse
  :config (eyebrowse-mode t)
  :straight t)

(use-package winum
  :after spaceline
  :bind (("M-1" . winum-select-window-1)
	 ("M-2" . winum-select-window-2)
	 ("M-3" . winum-select-window-3)
	 ("M-4" . winum-select-window-4)
	 ("M-5" . winum-select-window-5)
	 ("M-6" . winum-select-window-6)
	 ("M-7" . winum-select-window-7)
	 ("M-8" . winum-select-window-8))
  :config (progn
            (setq winum-auto-setup-mode-line nil)
            (winum-mode))
  :demand t
  :straight t)

(use-package ace-window
  :bind (("C-x o" . ace-window))
  :demand t
  :straight t)

(use-package golden-ratio
  :after winum ace-window
  :config (progn
            (golden-ratio-mode 1)
            (advice-add 'winum--switch-to-window :after 'golden-ratio)
            (advice-add 'ace-window :after 'golden-ratio))
  :straight t)

;; UI customization
(blink-cursor-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(global-hl-line-mode 1)
(setq inhibit-startup-screen t
      ring-bell-function 'ignore)
(if (eq system-type 'gnu/linux)
    (set-frame-font "Source Code Pro-14" nil t)
    (set-frame-font "Source Code Pro-16" nil t))
(defalias 'yes-or-no-p 'y-or-n-p)
