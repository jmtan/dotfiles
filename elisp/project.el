(use-package magit
  :commands magit-status
  :bind (("C-x g" . magit-status))
  :config (progn (define-key magit-mode-map (kbd "M-1") nil)
		 (define-key magit-mode-map (kbd "M-2") nil)
		 (define-key magit-mode-map (kbd "M-3") nil)
		 (define-key magit-mode-map (kbd "M-4") nil)
		 (define-key magit-mode-map (kbd "1") 'magit-section-show-level-1-all)
		 (define-key magit-mode-map (kbd "2") 'magit-section-show-level-2-all)
		 (define-key magit-mode-map (kbd "3") 'magit-section-show-level-3-all)
		 (define-key magit-mode-map (kbd "4") 'magit-section-show-level-4-all))
  :straight t)

(use-package projectile
  :bind-keymap (("C-c p" . projectile-command-map)
		("C-c C-p" . projectile-command-map))
  :config (progn
	    (setq projectile-project-search-path '("~/Workspace"
						   "~/Workspace/radioactive"
						   ;; "/data/jmtan/Workspace/players"
						   )
		  projectile-completion-system 'ivy
		  projectile-switch-project-action #'projectile-dired)
	    (projectile-mode +1))
  :straight t)

(use-package treemacs
  :commands treemacs
  :after doom-themes
  :bind (("M-0" . treemacs-select-window)
	 ("C-x t t" . treemacs))
  :config (progn (doom-themes-treemacs-config))
  :straight t)

(use-package treemacs-projectile
  :commands treemacs-projectile
  :bind (:map treemacs-mode-map
         ("C-p p" . treemacs-projectile))
  :after treemacs projectile
  :straight t)
