(use-package org
  :config (progn
            (require 'ox-md nil t)
            (add-to-list 'org-mode-hook #'auto-fill-mode)
            (if (eq system-type 'gnu/linux)
              (setq org-agenda-files (quote ("/data/jmtan/Dropbox/org")))
              (setq org-agenda-files (quote ("~/Dropbox/org")))))
  :straight t)

(use-package org-pomodoro
  :commands org-pomodoro
  :straight t)

(use-package ox-jira
  :after org
  :straight t)

(use-package f
  :defer t
  :straight t)

(defun insert-date-time ()
  (insert (format-time-string "%H%M")))

;; https://stackoverflow.com/questions/6997387/how-to-archive-all-the-done-tasks-using-a-single-command
(defun org-archive-done-tasks ()
  (interactive)
  (org-map-entries
   (lambda ()
     (org-archive-subtree)
     (setq org-map-continue-from (outline-previous-heading)))
   "/DONE" 'file))

(require 'f)

(defun month-num (month-name)
  (cond
   ((string-equal "January" month-name) "01")
   ((string-equal "February" month-name) "02")
   ((string-equal "March" month-name) "03")
   ((string-equal "April" month-name) "04")
   ((string-equal "May" month-name) "05")
   ((string-equal "June" month-name) "06")
   ((string-equal "July" month-name) "07")
   ((string-equal "August" month-name) "08")
   ((string-equal "September" month-name) "09")
   ((string-equal "October" month-name) "10")
   ((string-equal "November" month-name) "11")
   ((string-equal "December" month-name) "12")))

(defun org-export-journal (dest-dir)
  (interactive)
  (let ((year nil)
        (month nil)
        (day nil)
        (header nil)
        (total 0))
    (org-map-entries
     (lambda ()
       (cond
        ((= 1 (org-current-level)) (setq year (org-get-heading)))
        ((= 2 (org-current-level)) (setq month (org-get-heading)))
        ((= 3 (org-current-level)) (setq day (org-get-heading)))
        ((= 4 (org-current-level)) (setq header (org-get-heading))))
       (if (= 4 (org-current-level))
           (let* ((header-elems (split-string header))
                  (time (car header-elems))
                  (title (string-join (rest header-elems) " "))
                  (date (format "%s-%s-%s" year (month-num month) day))
                  (filename (format "%s-%s.org" date header))
                  (dest (f-join dest-dir filename)))
             (message "writing %s" dest)
             (setq total (+ total 1))
             (f-write-text (format "#+TITLE: %s\n#+DATE: %s\n" title date) 'utf-8 dest)
             (f-append-text (org-get-entry) 'utf-8 dest))))
     t 'file)
    (message "%d posts created at %s" total dest-dir)))

